
# Task Set 2

#### Deadline: 06th October 2018(Saturday) 

|Task Number|Topic|Assignee (slack handle)|GitHub|Status|
|---|---|---|---|---|
|1.|fetch API ||||
|2.|HTTP 1.1 vs HTTP 2.0||||
|3.|IndexDB with Promise||||
|4.|CSS3 Grid||||
|5.|Service Workers and Benefits||||
|6.|Flutter||||
|7.|Progressive Web Apps||||
|8.|What's new in Angular 6||||
|9.|Basics Of Git and Github||||
|11.|Kotlin and its Benefits||||
|12.|What's New in Android Pie||||
