
# Task Set 1

#### Deadline: 15th September 2018(Saturday) 

|Task Number|Topic|Assignee (slack handle)|GitHub|Status|
|---|---|---|---|---|
|1.|fetch API |||:x:|
|2.|HTTP 1.1 vs HTTP 2.0|||:x:|
|3.|IndexDB with Promise|||:x:|
|4.|CSS3 Grid|||:x:|
|5.|Service Workers and Benefits|||:x:|
|6.|[React Basics](https://medium.com/udacity-google-india-scholars/react-basics-d82f65060231)|@Karthikeyan.mws||:heavy_check_mark:|
|7.|Progressive Web Apps|||:x:|
|8.|What's new in Angular 6|||:x:|
|9.|Basics Of Git and Github|||:x:|
|10.|Functional Programming with Javascript|||:x:|
